'use strict';

//
// Include Gulp & tools we'll use

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

//
// Compile scripts

gulp.task('scripts', function () {
    // For best performance, don't add Sass partials to `gulp.src`
    return gulp.src([
        'app/*/*.js',
        'app/app.js'
    ])
        // Concatenate and minify styles
        .pipe($.concat('app.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('build'));
});

//
// Compile stylesheets

gulp.task('styles', function () {
    return gulp.src([
        'app/*/*.css',
        'app/app.css'
    ])
        // Concatenate and minify styles
        .pipe($.concat('app.min.css'))
        .pipe($.csso())
        .pipe(gulp.dest('build'));
});

//
// Replace script and style reference with minified

gulp.task('html', function () {

    return gulp.src('index.html')
        .pipe($.htmlReplace({
            'css': 'app.min.css',
            'js': 'app.min.js'
        }))
        .pipe(gulp.dest('build'));
});

//
// Copy the images

gulp.task('images', function () {

    return gulp.src('**/*.png')
        .pipe($.flatten())
        .pipe(gulp.dest('build/images'));
});

//
// Clean output directory

gulp.task('clean', function () {
    return gulp.src('build/*', {read: false})
        .pipe($.clean());
});

// Build production files, the default task
gulp.task('default', ['clean', 'scripts', 'styles', 'images', 'html']);